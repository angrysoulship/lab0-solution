package Lab0;

public class Square {


    // To print filled rectangle
    static void print_square(int a)
    {
        int b, c;
 
        // Outer loop for rows, make sure the length.
        for (b = 0; b < a; b++) {
        	// Inner loop for column, make sure the width.
        	for(c = 1; c < a; c++) {
        		System.out.print('S');
        	}
        	System.out.println('S');	
            }
        
      

    
    }
 
    // Main method
    public static void main(String args[])
    {
        // Declaring the size

        int rows = 4;
        
        // calling the print_square method to print
        print_square(rows);
    }
};

